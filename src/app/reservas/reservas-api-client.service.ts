import { Injectable } from '@angular/core';
import { getAllDebugNodes } from '@angular/core/src/debug/debug_node';

@Injectable({
  providedIn: 'root'
})
export class ReservasApiClientService {

  constructor() { }


  getAll() {
    return [ { id: 1, name: 'uno'}, { id: 2, name: 'dos'}];
  }
}
