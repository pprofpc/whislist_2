import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	selected:boolean;
	servicios:string[];
	id = uuid();
	constructor(public nombre:string, public imagenUrl:string, public votes: number = 0 ) {
       this.servicios = ['pileta', 'desayuno'];
			 if (this.imagenUrl ==''){
				 this.imagenUrl = 'https://www.expreso.info/files/styles/large/public/2020-04/Viaje_Combinado.jpg?itok=RGKXCS0T';
			 		}
	}
	setSelected(s:boolean){
	  this.selected = s;
	}
	isSelected(){
	  return this.selected;
	}
	voteUp() {
	this.votes++ ;
	}

	voteDown() {
	this.votes-- ;
	}

	voteClean() {
		this.votes=0;
	}
}
