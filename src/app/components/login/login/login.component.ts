import { Component, OnInit } from '@angular/core';
import { bind } from '@angular/core/src/render3/instructions';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mmensajeError: string;

  constructor(public authService: AuthService) {
    this.mmensajeError = '';
   }

  ngOnInit() {
  }

  login(username: string, password:string): boolean{
    this.mmensajeError = '';
    if (!this.authService.login(username,password)){
      this.mmensajeError = 'Login incorrecto.' ;
      setTimeout(function(){
        this.mmensajeError = '';
      }.bind(this), 2500);
    }
    return false;
  }

  logout(): boolean{
    this.authService.logout();
    return false;
  }

}
